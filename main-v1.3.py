import random
import subprocess
import socket
import scipy.io

import warnings
import matplotlib
import cmath, math
matplotlib.use('TkAgg')
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure
import tkinter as tk  # python 2.7
from onePortVnaFunc import *
from tkinter import filedialog

class Application(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self,master)
        self.createWidgets()

    def createWidgets(self):
        fig=plt.figure(figsize=(8, 7))
        ax = fig.add_axes([0.1, 0.1, 0.8, 0.8], polar=True)
        ax.set_ylim([0,1])
        canvas = FigureCanvasTkAgg(fig, master=root)
        canvas.get_tk_widget().pack()
        canvas.get_tk_widget().place(x = 0, y =0)
        canvas.draw()
        self.plotbutton=tk.Button(master=root, text="Measure",  activebackground='#345',activeforeground='white',padx = 73, pady = 10,  command=lambda: self.plot(canvas, ax))
        self.plotbutton.pack()
        self.plotbutton.place(x = 800, y = 0)
        self.saveButton = tk.Button(master = root,padx = 67.5, activebackground='#345',activeforeground='white', pady = 9, text = "Save *.mat", command = lambda: self.askSaveAs(freqVec, s11Data))
        self.saveButton.place(x = 800, y = 53)
        self.measurementCnt = tk.Label(master = root, text = "Measurement\t %s" % 0, relief = 'sunken', padx = 10)
        self.measurementCnt.pack()
        self.yplc = 710
        self.measurementCnt.place(x = 0, y = self.yplc)
        self.ClearButton= tk.Button(master = root, text = "Clear All",activebackground='#345',activeforeground='white',padx = 73, pady = 10,  command=lambda: self.clear(canvas, ax))
        self.ClearButton.place(x=800, y=103)

    def plot(self,canvas,ax):
        c = ['r', 'b', 'g', 'c', 'm', 'y', 'k']  # plot marker colors
        ax.clear()         # clear axes from previous plot
        X, Y, S11 = getS11(vna, numFreqPoints)
        theta, r = getPhaseMag(S11)
        cidx = random.randrange(0,len(c), 1)
        ax.plot(theta, r, linestyle="solid", color = c[cidx])
        ax.set_ylim([0, 1])
        canvas.draw()

        global measCount
        global s11Data
        global freqVec
        if measCount == 1:
            s11Data = np.stack((S11))
        else:
            s11Data = np.vstack((s11Data, S11))
        # print(s11Data.shape)
        measCount = measCount+1
        self.measurementCnt = tk.Label(master = root, text = "Measurement\t %s" % (int(measCount)-1), relief = 'sunken', padx = 10)
        self.measurementCnt.pack()
        self.measurementCnt.place(x = 0, y = self.yplc)


    def saveDataWin(self, freqVec, s11Data):
        newWin = tk.Toplevel(master = root)
        newWin.geometry('390x80')
        newWin.title('Save *.Mat File')
        diaEnt = tk.Label(newWin, text = 'Save as filename:')
        diaEnt.place(x = 40, y = 25)
        entry = tk.Entry(newWin, bd = 3)
        entry.place(x = 145, y = 25)
        saveButton = tk.Button(newWin, text = 'Save', padx = 10, command = lambda: self.save(newWin, entry.get(), freqVec, s11Data), )
        saveButton.place(x = 300, y = 24)

    def save(self,newWin, fileName, freqVec, s11Data):
        errMsg = tk.Label(newWin, text='No filename entered')
        errMsg.config(fg = 'red')
        if not fileName:
            # errMsg.place(x = 180, y = 0)
            warn = tk.messagebox.showwarning(message = 'No filename entered')
            newWin.destroy()
        else:
            scipy.io.savemat(fileName+'.mat', {'Parameters': s11Data, 'Frequency': freqVec})
        newWin.destroy()

    def askSaveAs(self, freqVec, s11Data):
        newWin = tk.Toplevel(master = root)
        newWin.withdraw()
        directory = filedialog.asksaveasfile(defaultextension=".mat")
        dataToSave = {'frequency': freqVec, 'sparameters': s11Data}
        scipy.io.savemat(directory.name, dataToSave, appendmat=True)
        base = directory.name.split('.')[0]
        np.savez(base, frequency=freqVec, sparameters =  s11Data)

    def clear(self, canvas, ax):
        global measCount
        global s11Data
        measCount = 1
        ax.clear()
        canvas.draw()
        self.measurementCnt = tk.Label(master=root, text="Measurement\t %s" % (int(measCount) - 1), relief='sunken',
                                       padx=10)
        self.measurementCnt.pack()
        self.measurementCnt.place(x = 0, y = self.yplc)
        s11Data = np.zeros(numFreqPoints, dtype=np.complex128)

def getPhaseMag(S11):
    global numFreqPoints
    idx = 0
    theta = np.zeros(numFreqPoints, dtype = float)
    mag = np.zeros(numFreqPoints, dtype = float)
    for i in S11:
        theta.flat[idx] = cmath.phase(i)
        mag.flat[idx] = abs(i)
        idx = idx + 1
    return theta, mag


# subprocess.Popen(r'C:\Program Files (x86)\Anritsu Company\ShockLine\Application\AC_GUIMain.exe')

# # Check if Shockline is running, starts if not and waits until s is true?
#
# def checkShockline():
#     s = subprocess.check_output('tasklist', shell=True)
#     if 'AC_GUIMain.exe' in str(s):
#         shockLine = True
#     else:
#         shockLine = False
#         print('ShockLine is Not Running')
#     return shockLine
#
# subprocess.Popen(r'C:\Program Files (x86)\Anritsu Company\ShockLine\Application\AC_GUIMain.exe')
# print('Starting Shockline')
#
# checkShockline()

measCount = 1
vna = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
vna.connect(("127.0.0.1", 5001))
vna.send(str.encode("*IDN?\n"))
tmp = vna.recv(200)
# parse return byte string
tmpstr = str(tmp)
splitTmp = tmpstr.split(",")
# print(splitTmp)
for i in splitTmp:
    if i == 'MS46131A':
        onePortFlag = 1
        instrName = i

print('Connected to ' + instrName)
# check calibration
vna.send(str.encode(":SENS:CORR:STAT?\n"))
rec = vna.recv(2000)
rec = rec.rstrip()
calStatus = rec.decode()
if int(calStatus) == 0:
    warnings.warn(instrName + " VNA Not Calibrated")

numFreqPoints = getnumF(vna)
s11Data = np.ndarray(numFreqPoints, dtype=np.complex128)

# required setup
vna.send(str.encode(":FORM:DATA REAL32::=FMB \n"))
vna.send(str.encode(":FORM:BORDer NORMAL\n"))  # MSB

freqVec = getFreq(vna, numFreqPoints)

def quit_me():
    root.quit()
    root.destroy()

root=tk.Tk()
root.title('Read VNA version 1.3')
root.geometry('1000x750')
root.protocol("WM_DELETE_WINDOW", quit_me)
app = Application(master=root)
app.mainloop()