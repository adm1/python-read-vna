import time
import numpy as np
from struct import *
import socket
def getFreq(vna,numFreqPoints):
    # get frequency points (real32 4 byte data)
    vna.send(str.encode(":SENS:FREQ:DATA?\n"))
    header = vna.recv(100)
    header = header.decode('utf-8')
    buffer = int(header.lstrip('#9'))
    # print(buffer)
    freqData = vna.recv(buffer + 1, socket.MSG_WAITALL)  # plus one for new line
    freqData = freqData[:-1]
    hexFreq = freqData.hex(' ', 4)

    freqV = np.zeros(numFreqPoints, dtype=float)
    hexFreq = hexFreq.split(' ')
    idx = 0
    for i in hexFreq:
        real32 = bin(int(i, 16))
        freqV.flat[idx] = unpack('f', pack('I', int(real32, 2)))[0]
        idx = idx + 1
    # print(freqV)
    return freqV

def getS11(vna, numFreqPoints):
    numDP = 2   # number of data parameters
    BOPN = 4    # single precision binary
    dataBlockSize = numDP * BOPN * numFreqPoints
    vna.send(str.encode(":SENSe:HOLD:FUNC SING\n"))
    vna.send(str.encode(":TRIG SING\n"))  # trigger 1 measurement
    time.sleep(1)   # gives time for VNA to Sweep
    vna.send(str.encode(":CALC:PAR:DATA:SDAT?\n"))
    #   Get buffer size
    header = vna.recv(11, socket.MSG_WAITALL)
    # print(header)
    try:
        header = header.decode('utf-8')
        buffer = int(header.lstrip('#9'))
        # print('Buffer size %s' % buffer)
        # if buffer == dataBlockSize:
        #     print("buffer is correct")
    # print(header)
    # print("S parameter buffer size " + str(buffer))
    except:
        # print('header incorrect read')
        buffer = dataBlockSize
        vna.send(str.encode(":CALC:PAR:DATA:SDAT?\n"))
    finally:
        data = vna.recv(buffer + 1, socket.MSG_WAITALL)     # plus one fore newline
        data = data[:-1]
        hexSpar = data.hex(' ', 4)
        sParV = np.zeros(numFreqPoints*2, dtype=float)
        hexSpar = hexSpar.split(' ')
        idx = 0
        for i in hexSpar:
            real32 = bin(int(i, 16))
            tmp = unpack('f', pack('I', int(real32, 2)))[0]
            sParV.flat[idx] = tmp
            if idx < numFreqPoints*2:
                idx = idx + 1
            else:
                idx = idx
        S11 = np.ndarray(numFreqPoints, dtype=np.complex128)
        realIdx = np.arange(0, (numFreqPoints)*2, 2)
        imagIdx = np.arange(1, (numFreqPoints)* 2, 2)
        S11 = sParV[realIdx] + 1j * sParV[imagIdx]
        X = [x.real for x in S11]
        Y = [y.imag for y in S11]
    return X, Y, S11

def getnumF(vna):
    vna.send(str.encode(":SENSe:SWEEP:POINT?\n"))
    resp = vna.recv(100)
    resp = resp.decode('utf-8')
    numPoints = int(resp)
    return numPoints